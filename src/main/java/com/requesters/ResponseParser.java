 package com.requesters;

import org.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sllogger.TrainInfo;

public class ResponseParser {
	private TrainInfo[] trainInfo;
public TrainInfo[] parseSLResponse(String response) {

	
		try {
				
	    JsonParser parser = new JsonParser();
	    JsonArray jArray=parser.parse(response).getAsJsonArray();
	    
	   
	    int noOfTrains=jArray.size();
	    trainInfo=new TrainInfo[noOfTrains];//CREATEING ARRAY OF A GIVEN SIZE 
	    
	    for (int i=0; i<noOfTrains;i++) {
	    
	    	JsonObject trainInfoObj=jArray.get(i).getAsJsonObject();
	    	trainInfo[i]=new TrainInfo();  //INITIALIZING EACH ARRAY
	    	
	    trainInfo[i].setTrainNumber(trainInfoObj.get("trainNumber").getAsString());
	    trainInfo[i].setLineNumber(trainInfoObj.get("lineNumber").toString().replaceAll("\"", ""));
	    trainInfo[i].setFromStation(trainInfoObj.get("fromStation").toString().replaceAll("\"", ""));
	    trainInfo[i].setToStation(trainInfoObj.get("toStation").toString().replaceAll("\"", ""));
	    trainInfo[i].setTrainStationName(trainInfoObj.get("trainStationName").toString().replaceAll("\"", ""));
	    trainInfo[i].setAdvertisedDepartureTime(trainInfoObj.get("advertisedDepartureTime").toString().replaceAll("\"", ""));
	    trainInfo[i].setAdvertisedDepartureTime(trainInfoObj.get("timeUntilDeparture").toString().replaceAll("\"", ""));
	    trainInfo[i].setCurrentStation(trainInfoObj.get("currentStation").toString().replaceAll("\"", ""));
	    trainInfo[i].setTrainHasLeftCurrentStation(trainInfoObj.get("trainHasLeftCurrentStation").getAsBoolean());
	    trainInfo[i].setTrainArrivedOrLeftCurrentStation(trainInfoObj.get("trainArrivedOrLeftCurrentStation").toString().replaceAll("\"", ""));
	    trainInfo[i].setTrainArrivedOrLeftCurrentStationDuration(trainInfoObj.get("trainArrivedOrLeftCurrentStationDuration").toString().replaceAll("\"", ""));
	    trainInfo[i].setHasDeviation(trainInfoObj.get("hasDeviation").getAsBoolean());
	    trainInfo[i].setTimeUntilDepartureIncludingDeviation(trainInfoObj.get("timeUntilDepartureIncludingDeviation").toString().replaceAll("\"", ""));
	    trainInfo[i].setEstimatedTimeAtLocation(trainInfoObj.get("estimatedTimeAtLocation").toString().replaceAll("\"", ""));
	    trainInfo[i].setTimeUntilDepartureByEstimatedTimeAtLocation(trainInfoObj.get("timeUntilDepartureByEstimatedTimeAtLocation").toString().replaceAll("\"", ""));

	    }
  
	    return trainInfo;
	
		}
		catch(JSONException ex) {
			ex.printStackTrace();
			//return "Warning!!! Couldn't set to PrettyJson - Not Valid Json -->"+response;
			return null;
		}
	

	
}
}
