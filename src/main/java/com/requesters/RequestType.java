package com.requesters;

public enum RequestType {
	GET,
	POST,
	PUT,
	DELETE
}
