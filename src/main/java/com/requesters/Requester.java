package com.requesters;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

import org.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sleslogger.ElasticLogger;
import com.sllogger.TrainInfo;

public class Requester {
	private static Requester ownInstance=null;
	final ElasticLogger el=new ElasticLogger();
	
	//public MyLogger myLogger=MyLogger.getInstance();
	private int responseCode;
	private String host;
	private long startTime;
	private long exTime;
	 
	private Requester() {
		setDefaults();
	}
	
	private void setDefaults() {
		
		this.host="https://www.pendelkoll.se";
		
		
	}
	public static Requester getInstance() {
		if (ownInstance == null) ownInstance=new Requester();
		return ownInstance;
	}

public String request(String param, String method,String surl,String session) {
	
	String responseMessage = null;
	try {
		this.startTime = System.currentTimeMillis();	
		String urlString2Decode = host+surl;
	
		URI uri=null;
		try {
			uri = new URI(urlString2Decode);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String asciiURI=uri.toASCIIString();
		URL url = new URL(asciiURI);
		
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setDoInput(true);
	    conn.setRequestMethod(method);
		conn.setRequestProperty("Content-length", "0");
		conn.setRequestProperty("Accept-Charset", "UTF-8"); 
		conn.setDoOutput(true);
		if (session!=null) 
			conn.setRequestProperty("Cookie", session);
		if (method.matches("POST") || method.matches("PUT")) {
		byte[] paramData=param.getBytes();
		conn.getOutputStream().write(paramData);
		}
		
		conn.connect();
	    responseCode= conn.getResponseCode();
	    InputStream stream = conn.getErrorStream();
	    if (stream == null) {
	        stream = conn.getInputStream();
	    }
	    try (Scanner scanner = new Scanner(stream)) {
	        scanner.useDelimiter("\\Z");
	        responseMessage = scanner.next();
	    }
	    catch(Exception e) {
			e.printStackTrace();
		}
	    this.exTime=System.currentTimeMillis()-startTime;
	} catch (MalformedURLException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
requestPrinter (surl,method,param,responseCode,responseMessage,this.exTime);
	return responseMessage;
}


public void requestPrinter(String url,String method,String param,int responseCode,String responseMessage,long exTime) {
	String requestSummary="\n";
	
	requestSummary=requestSummary.concat("RequestURL: "+host+url+"\n");
	requestSummary=requestSummary.concat("RequestMethod: "+method+"\n");
	requestSummary=requestSummary.concat("ResponseCode: "+responseCode+"\n");
	//requestSummary=requestSummary.concat("ResponseMessage: "+toPrettyJson(responseMessage)+"\n");
	requestSummary=requestSummary.concat("ResponseMessage: "+responseMessage+"\n");
	
	//myLogger.log(requestSummary);
	
	
	ResponseParser responseParser=new ResponseParser();
	TrainInfo[] trainInfo=responseParser.parseSLResponse(responseMessage);
	
	//i will change the responseMessage below with trainInfo then change the sllogger according to the class
	el.logToEs(responseCode,host,url,method,param,trainInfo);
//	System.out.println(requestSummary);
//return requestSummary;
	
}

public String toPrettyJson(String jsonString) 
{
	try {
    JsonParser parser = new JsonParser();
    JsonObject json = parser.parse(jsonString).getAsJsonArray().getAsJsonObject();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String prettyJson = gson.toJson(json);
    
    return prettyJson;
	}
	catch(JSONException ex) {
		ex.printStackTrace();
		return "Warning!!! Couldn't set to PrettyJson - Not Valid Json -->"+jsonString;
	}
}


}
