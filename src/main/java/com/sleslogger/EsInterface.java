package com.sleslogger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.client.transport.TransportClient;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import static org.elasticsearch.common.xcontent.XContentFactory.*;
import org.elasticsearch.transport.client.PreBuiltTransportClient;


public class EsInterface {
	
    private Hashtable <String,Boolean> indexOK = new  Hashtable<String, Boolean>();
    
	private static EsInterface ownInstance = null;
	private boolean interfaceOpen = false;
	
	private EsInterface (){
	    
	}
	
   	public static EsInterface getInstance (){
   		if (ownInstance == null) ownInstance = new EsInterface ();
   		return ownInstance;
   	}
	
   	private TransportClient client = null;
   	
	private static String hostName = "localhost";
	private static int hostPort = 9300;
	
//	static String dbIndex = "myLoggerindex";
	static String dbType = "slloggerdb";
	static String dbIndex = "slloggerindex";
//	static String dbType = "myType";
   	
	
	public static void setEsInterfaceParameters (String hname, int hport,String dType,String dIndex){
		
		hostName = hname;
		hostPort = hport;
		dbType=dType;
		dbIndex=dIndex;
		
	}
	
	public boolean isOpen (){
		return interfaceOpen;
	}
   	
	@SuppressWarnings("resource")
	public boolean openInterFace (String serverName, int serverPort){
		 
	    if (interfaceOpen == true) return true;

		try {			
					
			
			Settings settings =Settings.builder().put("cluster.name", "elasticsearch").build();			
			client = new PreBuiltTransportClient(settings)
					.addTransportAddress(new TransportAddress(InetAddress.getByName(serverName), serverPort));
	        //.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("host2"), 9300));
	        
			
			
			/*
			String timeIndex = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

			if (indexOK.containsKey(timeIndex) == false){
				
				// CREATE THE INDEX
				boolean errorFound = setupIndexNew (timeIndex);	
				//boolean errorFound = setupIndex (timeIndex);	

				if (errorFound == false) interfaceOpen = true;
				else interfaceOpen = false;
				
			}*/
			
			interfaceOpen = true;
			
		
	    } catch (NoNodeAvailableException e) {
		    e.printStackTrace();
		    interfaceOpen = false;
	    } catch (ExceptionInInitializerError e) {
		    e.printStackTrace();
		    interfaceOpen = false;
	    } catch (Exception e) {
		    e.printStackTrace();
		    interfaceOpen = false;
	    }		

	    return interfaceOpen;
		
	}
	
	
	public boolean logEvent (Long timeStamp, boolean status, int exTime,Map<String,Object> jsonDocument){
	    
		if (interfaceOpen == false) return false;
		
		boolean success = false;
		String timeIndex = getDate (timeStamp);
		
		if (indexOK.containsKey(timeIndex) == false){
			// CREATE THE INDEX
		    success = setupIndex (timeIndex);	
	
		}
		else success = true;
		
		if (success == true){
			
	       
	    	        
	        return storeEvent (timeIndex,jsonDocument);
			
		}
		else System.out.print("\nSETUP failed");
		
		return false;
		
	}
	

	public boolean logErrorEvent (Long timeStamp, boolean status, Long exTime){
		
        if (interfaceOpen == false) return false;
        
        boolean success = false;
        String timeIndex = getDate (timeStamp);

        if (indexOK.containsKey(timeIndex) == false){
        
            // CREATE THE INDEX
            success = setupIndex (timeIndex);    
        
        }
        else success = true;

        if (success == true){
            
            Map<String, Object> jsonDocument = new HashMap<String, Object>();
           
            jsonDocument.put("timestamp", adjustTimestamp(timeStamp));
            jsonDocument.put("api", "echonodejs");
            if (status==true) jsonDocument.put("SUCCESS", 1);
            else jsonDocument.put("ERROR", 1);
            jsonDocument.put("exTime", exTime);
                    
            return storeEvent (timeIndex,jsonDocument);
            
        }
        
        return false;
		
	}
	
	private boolean setupIndexNew (String timeIndex){
		
		try {
					
			IndicesExistsResponse res =  client.admin().indices().prepareExists((dbIndex +"-"+timeIndex)).execute().actionGet();
		
			if (res.isExists() == false){
	        	 		            
			    CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate((dbIndex +"-"+timeIndex));		       		           
	               		            
			    XContentBuilder mappingBuilder = null;					
			    try {
											
				    mappingBuilder = jsonBuilder().startObject().startObject(dbType)
					                 .startObject("properties").startObject("timestamp")
					                 .field("type", "date").field("format", "yyyy-MM-dd'T'HH:mm:ss.SSS")
					                 .endObject().endObject().endObject()
					                 .endObject();
																	
			
				    //System.out.print("\n\n"+mappingBuilder.string());				           			               
			        createIndexRequestBuilder.addMapping(dbType, mappingBuilder);
		               
			        // MAPPING DONE			               
			        createIndexRequestBuilder.execute().actionGet();
		
			    } catch (IOException e) {
				    // TODO Auto-generated catch block
			        //e.printStackTrace();
			        return false;
		        } catch (Exception e) {
				    // TODO Auto-generated catch block
			        //e.printStackTrace();
			        return false;
		        }
	    
		    }

	        // STORE CREATED OR EXISTING INDEX
		    indexOK.put(timeIndex, true);

		    return true;

		} catch (Exception e){
		    //e.printStackTrace();
			return false;
		}
		
	}
	
	
	private boolean setupIndex (String timeIndex){
		
		try {
		
			IndicesExistsResponse res =  client.admin().indices().prepareExists((dbIndex +"-"+timeIndex)).execute().actionGet();
		
			if (res.isExists() == false){
	        	 		            
			    CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate((dbIndex +"-"+timeIndex));		       		           
	               		            
			    XContentBuilder mappingBuilder = null;					
			    try {
											
				    mappingBuilder = jsonBuilder().startObject().startObject(dbType)
					                 .startObject("properties").startObject("timestamp")
					                 .field("type", "date").field("format", "yyyy-MM-dd'T'HH:mm:ss.SSS")
					                 .endObject().endObject().endObject()
					                 .endObject();
																	
			
				    //System.out.print("\n\n"+mappingBuilder.string());				           			               
			        createIndexRequestBuilder.addMapping(dbType, mappingBuilder);
		               
			        // MAPPING DONE			               
			        createIndexRequestBuilder.execute().actionGet();
		
			    } catch (IOException e) {
				    // TODO Auto-generated catch block
			        e.printStackTrace();
			        return false;
		        } catch (Exception e) {
				    // TODO Auto-generated catch block
			        e.printStackTrace();
			        return false;
		        }
	    
		    }

	        // STORE CREATED OR EXISTING INDEX
		    indexOK.put(timeIndex, true);

		    return true;

		} catch (Exception e){
		    //e.printStackTrace();
			return false;
		}
		
	}
	
	private boolean storeEvent (String timeIndex, Map<String, Object> document){

		try{
		
			IndexResponse response = client.prepareIndex((dbIndex +"-"+timeIndex), dbType).setSource(document).execute().actionGet();

		    if (response != null){
	 	    	   
	 	        if (response.getVersion()>0L){
	 	    	
	 	           // ADD SOME LOGGING OR SOMETHING TO SHOW THAT ONE USE THE SAME ID. (Duplicates)
	 	           return true;
	 	    		
	 	        }
	 	        else{

	 	    	    System.out.print ("\nERROR\tFailed to store data in ElasticSearch : "+dbIndex +"\t" +dbType +"\t"+timeIndex+"\tResponse = "+response.toString());
		 	        return false;
	 	    
	 	        }	
	 	     
	 	    }
	 	
	 	    return true;
	
		} catch (Exception e){
	        e.printStackTrace();
	        return true;
		}	 	 
		
	}


	
	public void closeInterface (){
		if (client != null) client.close();
		interfaceOpen = false;
		ownInstance = null;
	}

	   private String getDate (Long timestamp){
	       
//		   LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault());  
//	       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
//	       return date.format(formatter);
	     
	        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date timeNow = new Date(); 
			String timeNowString=dateFormat.format(timeNow);
		//	System.out.println("newTimeStamp : "+timeNowString);
			return timeNowString;
	        	 
	    }
	
	public String adjustTimestamp (Long time){
		DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		
       // LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
  

        String timestamp = ZonedDateTime.now().format(formatter);
 
	    	    
		// adjust timestamp if needed
        if (timestamp.length()<23){
   	 
        	if (timestamp.length() == 19) timestamp = timestamp +".000";
   	        else if (timestamp.length() == 20) timestamp = timestamp +"000";
   	        else if (timestamp.length() == 20) timestamp = timestamp +"00";
   	        else if (timestamp.length() == 22) timestamp = timestamp +"0";

        }
        
        return timestamp;
 
	}

}

