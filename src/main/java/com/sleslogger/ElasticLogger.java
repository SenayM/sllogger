package com.sleslogger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import com.sllogger.TrainInfo;

public class ElasticLogger {

	public static void main(String[] args) {

	}

	// TO LOG FROM ANY OTHER APPLIATION
	public boolean logToEs(int resCode, String host, String url, String method, String request, TrainInfo[] trainInfo) {
		// GET INSTANCE
		EsInterface es = EsInterface.getInstance();
		boolean status = false;
		if (resCode == 200 || resCode == 201)
			status = true;

		// OPTIONAL SET ES INTERFACE
		EsInterface.setEsInterfaceParameters("localhost", 9300, "slelasticdb", "slelasticindex");

		// CONNECT TO DB IF NEEDED
		if (es.isOpen() == false) {

			if (es.openInterFace("localhost", 9300) == false)
				return false;
		}

		long timeStamp = getDate();
		
		//Iterate through the jsonArray to log each train info in the same station
		 for (int i=0; i<trainInfo.length;i++) {
		
		// GET DATA TO BE LOGGED
		Map<String, Object> jsonDocument = new HashMap<String, Object>();

		jsonDocument.put("timestampp", es.adjustTimestamp(getDate()));
		jsonDocument.put("host", host);
		jsonDocument.put("url", url);
		jsonDocument.put("request Method", method);
		jsonDocument.put("Request ", request);
		jsonDocument.put("trainNumber", trainInfo[i].getTrainNumber());
		jsonDocument.put("lineNumber", trainInfo[i].getLineNumber());
		jsonDocument.put("fromStation", trainInfo[i].getFromStation());
		
		jsonDocument.put("toStation",trainInfo[i].getToStation());
		jsonDocument.put("trainStationName",trainInfo[i].getTrainStationName());
		jsonDocument.put("advertisedDepartureTime",trainInfo[i].getAdvertisedDepartureTime());
		jsonDocument.put("timeUntilDeparture",trainInfo[i].getTimeUntilDeparture());
		jsonDocument.put("currentStation",trainInfo[i].getCurrentStation());
		jsonDocument.put("trainHasLeftCurrentStation",trainInfo[i].isTrainHasLeftCurrentStation());
		jsonDocument.put("trainArrivedOrLeftCurrentStation",trainInfo[i].getTrainArrivedOrLeftCurrentStation());
		jsonDocument.put("trainArrivedOrLeftCurrentStationDuration",trainInfo[i].getTrainArrivedOrLeftCurrentStationDuration());
		jsonDocument.put("hasDeviation",trainInfo[i].isHasDeviation());
		jsonDocument.put("timeUntilDepartureIncludingDeviation",trainInfo[i].getTimeUntilDepartureIncludingDeviation());
		jsonDocument.put("estimatedTimeAtLocation",trainInfo[i].getEstimatedTimeAtLocation());
		jsonDocument.put("timeUntilDepartureByEstimatedTimeAtLocation",trainInfo[i].getTimeUntilDepartureByEstimatedTimeAtLocation());

		if (status)
			jsonDocument.put("SUCCESS", 1);
		else
			jsonDocument.put("SUCCESS", 0);
		jsonDocument.put("Response Code", resCode);

		// LOG EVENT
		es.logEvent(timeStamp, status, resCode, jsonDocument);

//		System.out.println("\nTimeStamp :" + timeStamp + " LoggedTo Elastic :" + status + " ResponseCode :" + resCode);
//		System.out.println("dbType :" + es.dbType + "     dbIndex : " + es.dbIndex);
	

		 }
			return true;
	}

	// THE IDEA TO IMPLEMENT THIS METHOD IS ALL THE LOGGERS WILL USE THE SAME
	// STRUCUTE AND SET THE JSONDOCUMENT BY WHAT THEY WANT TO LOG
	// BY DEFAULT TIMESTAMP,STATUS AND EXECUTION TIME WILL BE LOGGED
	private void setJsonDocument(Map<String, Object> jsonDocument, String request, String response) {

		jsonDocument.put("api", "mukeraapi_01");
		jsonDocument.put("url", "/myMukera/log");
		jsonDocument.put("request Method", "GET");
		jsonDocument.put("Request ", request);
		jsonDocument.put("Response", response);

	}

	private static long getDate() {
		// Get current date time
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
		String formatDateTime = now.format(formatter);
		return Long.parseLong(formatDateTime);

	}

}
