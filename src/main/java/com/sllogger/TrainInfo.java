package com.sllogger;

public class TrainInfo {

	private String timeNow;
	private String timeNowRounded;
	private String trainNumber;
	private String lineNumber;
	private String fromStation;
	private String toStation;
	private String trainStationName;
	private String advertisedDepartureTime;
	private String timeUntilDeparture;
	private String currentStation;
	private boolean trainHasLeftCurrentStation;
	private String trainArrivedOrLeftCurrentStation;
	private String trainArrivedOrLeftCurrentStationDuration;
	private boolean hasDeviation;
	private String deviation;
	private String timeUntilDepartureIncludingDeviation;
	private String estimatedTimeAtLocation;
	private String timeUntilDepartureByEstimatedTimeAtLocation;
	private String timeUntilDeparture_AllConditions;
	private int numberOfUnvisitedStationsUntilSearchedStation;
	private int numberOfAlreadyVisitedStations;
	private Station visitedStations;
	
	/**
	 * @return the timeNow
	 */
	public String getTimeNow() {
		return timeNow;
	}
	/**
	 * @param timeNow the timeNow to set
	 */
	public void setTimeNow(String timeNow) {
		this.timeNow = timeNow;
	}
	/**
	 * @return the timeNowRounded
	 */
	public String getTimeNowRounded() {
		return timeNowRounded;
	}
	/**
	 * @param timeNowRounded the timeNowRounded to set
	 */
	public void setTimeNowRounded(String timeNowRounded) {
		this.timeNowRounded = timeNowRounded;
	}
	/**
	 * @return the trainNumber
	 */
	public String getTrainNumber() {
		return trainNumber;
	}
	/**
	 * @param trainNumber the trainNumber to set
	 */
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the fromStation
	 */
	public String getFromStation() {
		return fromStation;
	}
	/**
	 * @param fromStation the fromStation to set
	 */
	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}
	/**
	 * @return the toStation
	 */
	public String getToStation() {
		return toStation;
	}
	/**
	 * @param toStation the toStation to set
	 */
	public void setToStation(String toStation) {
		this.toStation = toStation;
	}
	/**
	 * @return the trainStationName
	 */
	public String getTrainStationName() {
		return trainStationName;
	}
	/**
	 * @param trainStationName the trainStationName to set
	 */
	public void setTrainStationName(String trainStationName) {
		this.trainStationName = trainStationName;
	}
	/**
	 * @return the advertisedDepartureTime
	 */
	public String getAdvertisedDepartureTime() {
		return advertisedDepartureTime;
	}
	/**
	 * @param advertisedDepartureTime the advertisedDepartureTime to set
	 */
	public void setAdvertisedDepartureTime(String advertisedDepartureTime) {
		this.advertisedDepartureTime = advertisedDepartureTime;
	}
	/**
	 * @return the timeUntilDeparture
	 */
	public String getTimeUntilDeparture() {
		return timeUntilDeparture;
	}
	/**
	 * @param timeUntilDeparture the timeUntilDeparture to set
	 */
	public void setTimeUntilDeparture(String timeUntilDeparture) {
		this.timeUntilDeparture = timeUntilDeparture;
	}
	/**
	 * @return the currentStation
	 */
	public String getCurrentStation() {
		return currentStation;
	}
	/**
	 * @param currentStation the currentStation to set
	 */
	public void setCurrentStation(String currentStation) {
		this.currentStation = currentStation;
	}
	/**
	 * @return the trainHasLeftCurrentStation
	 */
	public boolean isTrainHasLeftCurrentStation() {
		return trainHasLeftCurrentStation;
	}
	/**
	 * @param trainHasLeftCurrentStation the trainHasLeftCurrentStation to set
	 */
	public void setTrainHasLeftCurrentStation(boolean trainHasLeftCurrentStation) {
		this.trainHasLeftCurrentStation = trainHasLeftCurrentStation;
	}
	/**
	 * @return the trainArrivedOrLeftCurrentStation
	 */
	public String getTrainArrivedOrLeftCurrentStation() {
		return trainArrivedOrLeftCurrentStation;
	}
	/**
	 * @param trainArrivedOrLeftCurrentStation the trainArrivedOrLeftCurrentStation to set
	 */
	public void setTrainArrivedOrLeftCurrentStation(String trainArrivedOrLeftCurrentStation) {
		this.trainArrivedOrLeftCurrentStation = trainArrivedOrLeftCurrentStation;
	}
	/**
	 * @return the trainArrivedOrLeftCurrentStationDuration
	 */
	public String getTrainArrivedOrLeftCurrentStationDuration() {
		return trainArrivedOrLeftCurrentStationDuration;
	}
	/**
	 * @param trainArrivedOrLeftCurrentStationDuration the trainArrivedOrLeftCurrentStationDuration to set
	 */
	public void setTrainArrivedOrLeftCurrentStationDuration(String trainArrivedOrLeftCurrentStationDuration) {
		this.trainArrivedOrLeftCurrentStationDuration = trainArrivedOrLeftCurrentStationDuration;
	}
	/**
	 * @return the hasDeviation
	 */
	public boolean isHasDeviation() {
		return hasDeviation;
	}
	/**
	 * @param hasDeviation the hasDeviation to set
	 */
	public void setHasDeviation(boolean hasDeviation) {
		this.hasDeviation = hasDeviation;
	}
	/**
	 * @return the deviation
	 */
	public String getDeviation() {
		return deviation;
	}
	/**
	 * @param deviation the deviation to set
	 */
	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}
	/**
	 * @return the timeUntilDepartureIncludingDeviation
	 */
	public String getTimeUntilDepartureIncludingDeviation() {
		return timeUntilDepartureIncludingDeviation;
	}
	/**
	 * @param timeUntilDepartureIncludingDeviation the timeUntilDepartureIncludingDeviation to set
	 */
	public void setTimeUntilDepartureIncludingDeviation(String timeUntilDepartureIncludingDeviation) {
		this.timeUntilDepartureIncludingDeviation = timeUntilDepartureIncludingDeviation;
	}
	/**
	 * @return the estimatedTimeAtLocation
	 */
	public String getEstimatedTimeAtLocation() {
		return estimatedTimeAtLocation;
	}
	/**
	 * @param estimatedTimeAtLocation the estimatedTimeAtLocation to set
	 */
	public void setEstimatedTimeAtLocation(String estimatedTimeAtLocation) {
		this.estimatedTimeAtLocation = estimatedTimeAtLocation;
	}
	/**
	 * @return the timeUntilDepartureByEstimatedTimeAtLocation
	 */
	public String getTimeUntilDepartureByEstimatedTimeAtLocation() {
		return timeUntilDepartureByEstimatedTimeAtLocation;
	}
	/**
	 * @param timeUntilDepartureByEstimatedTimeAtLocation the timeUntilDepartureByEstimatedTimeAtLocation to set
	 */
	public void setTimeUntilDepartureByEstimatedTimeAtLocation(String timeUntilDepartureByEstimatedTimeAtLocation) {
		this.timeUntilDepartureByEstimatedTimeAtLocation = timeUntilDepartureByEstimatedTimeAtLocation;
	}
	/**
	 * @return the timeUntilDeparture_AllConditions
	 */
	public String getTimeUntilDeparture_AllConditions() {
		return timeUntilDeparture_AllConditions;
	}
	/**
	 * @param timeUntilDeparture_AllConditions the timeUntilDeparture_AllConditions to set
	 */
	public void setTimeUntilDeparture_AllConditions(String timeUntilDeparture_AllConditions) {
		this.timeUntilDeparture_AllConditions = timeUntilDeparture_AllConditions;
	}
	/**
	 * @return the numberOfUnvisitedStationsUntilSearchedStation
	 */
	public int getNumberOfUnvisitedStationsUntilSearchedStation() {
		return numberOfUnvisitedStationsUntilSearchedStation;
	}
	/**
	 * @param numberOfUnvisitedStationsUntilSearchedStation the numberOfUnvisitedStationsUntilSearchedStation to set
	 */
	public void setNumberOfUnvisitedStationsUntilSearchedStation(int numberOfUnvisitedStationsUntilSearchedStation) {
		this.numberOfUnvisitedStationsUntilSearchedStation = numberOfUnvisitedStationsUntilSearchedStation;
	}
	/**
	 * @return the numberOfAlreadyVisitedStations
	 */
	public int getNumberOfAlreadyVisitedStations() {
		return numberOfAlreadyVisitedStations;
	}
	/**
	 * @param numberOfAlreadyVisitedStations the numberOfAlreadyVisitedStations to set
	 */
	public void setNumberOfAlreadyVisitedStations(int numberOfAlreadyVisitedStations) {
		this.numberOfAlreadyVisitedStations = numberOfAlreadyVisitedStations;
	}
	/**
	 * @return the visitedStations
	 */
	public Station getVisitedStations() {
		return visitedStations;
	}
	/**
	 * @param visitedStations the visitedStations to set
	 */
	public void setVisitedStations(Station visitedStations) {
		this.visitedStations = visitedStations;
	}

	
	
	
	
	
}
