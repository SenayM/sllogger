package com.sllogger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.requesters.Requester;
import com.requesters.ResponseParser;

public class SlLogger {
	private static ResponseParser responseParser=new ResponseParser();
	private static Requester requester = Requester.getInstance();
	private static StationList stations=new StationList();
	private static String response=null;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Start here...");
		
	

		HashMap<String,String> stationList=stations.getStationList();
		
 
		for ( HashMap.Entry<String, String> station : stationList.entrySet()) 
		{ 
			System.out.println("stationName = " + station.getKey() + ", stationSignature = " + station.getValue()); 
			String url="/api/departure/station/"+station.getValue();	
		
			response=requester.request(null, "GET", url, null);
			responseParser.parseSLResponse(response);
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
//		
//		url="/api/departure/station/Arnc";
//		response=requester.request(null, "GET", url, null);	
//		responseParser.parseSLResponse(response);


	

		
		
	//	System.out.println(responseParser.parseSLResponse(response));
	}

}
