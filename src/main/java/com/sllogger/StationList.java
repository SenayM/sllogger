package com.sllogger;

import java.util.HashMap;
import java.util.Map;

public class StationList {
	
	HashMap<String,String> stationList=new HashMap<String,String>();
	
	public HashMap<String, String> getStationList() {
		return stationList;
	}
	public void setStationList(HashMap<String, String> stationList) {
		this.stationList = stationList;
	}
	
public StationList() {
	stationList.put("Älvsjö", "Äs");
	stationList.put("Arlanda C", "Arnc");
	stationList.put("Årstaberg", "Åbe");
	stationList.put("Bålsta", "Bål");
	stationList.put("Barkarby", "Bkb");
	stationList.put("Bro", "Bro");
	stationList.put("Farsta strand", "Fas");
	stationList.put("Flemingsberg", "Flb");
	stationList.put("Gröndalsviken", "Gdv");
	stationList.put("Häggvik", "Hgv");
	stationList.put("Handen", "Hnd");
	stationList.put("Helenelund", "Hel");
	stationList.put("Hemfosa", "Hfa");
	stationList.put("Huddinge", "Hu");
	stationList.put("Jakobsberg", "Jkb");
	stationList.put("Jordbro", "Jbo");
	stationList.put("Kallhäll", "Khä");
	stationList.put("Knivsta", "Kn");
	stationList.put("Krigslida", "Kda");
	stationList.put("Kungsängen", "Kän");
	stationList.put("Märsta", "Mr");
	stationList.put("Norrviken", "Nvk");
	stationList.put("Nynäsgård", "Ngd");
	stationList.put("Nynäshamn", "Nyh");
	stationList.put("Ösmo", "Öso");
	stationList.put("Östertälje", "Öte");
	stationList.put("Rönninge", "Rön");
	stationList.put("Rosersberg", "Rs");
	stationList.put("Rotebro", "R");
	stationList.put("Segersäng", "Ssä");
	stationList.put("Skogås", "Skg");
	stationList.put("Södertälje centrum", "Söc");
	stationList.put("Södertälje hamn", "Söd");
	stationList.put("Sollentuna", "Sol");
	stationList.put("Solna", "So");
	stationList.put("Spånga", "Spå");
	stationList.put("Stockholm City", "Sci");
	stationList.put("Stockholm Odenplan", "Sod");
	stationList.put("Stockholms södra", "Sst");
	stationList.put("Stuvsta", "Sta");
	stationList.put("Sundbyberg", "Sub");
	stationList.put("Trångsund", "Tåd");
	stationList.put("Tullinge", "Tul");
	stationList.put("Tumba", "Tu");
	stationList.put("Tungelsta", "Ts");
	stationList.put("Ulriksdal", "Udl");
	stationList.put("Upplands Väsby", "Upv");
	stationList.put("Uppsala C", "U");
	stationList.put("Västerhaninge", "Vhe");
}
}
